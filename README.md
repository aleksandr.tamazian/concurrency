# Concurrency

### JVM ThreadLocal
From javadoc:
>Each thread holds an implicit reference to its copy of a thread-local variable as long as the thread is alive and the ThreadLocal instance is accessible; after a thread goes away, all of its copies of thread-local instances are subject to garbage collection (unless other references to these copies exist).

`ThreadLocal` helps to avoid  weird issues in a multi-threaded environment, but how it actually implemented inside?
Let's look on `Thread` class:

```
public class Thread implements Runnable {
    ...
    /* ThreadLocal values pertaining to this thread. This map is maintained
     * by the ThreadLocal class. */
    ThreadLocal.ThreadLocalMap threadLocals = null;
    ...
}
```

`ThreadLocal` has a special mapper `ThreadLocalMap`, which associates each thread with their `ThreadLocal` instance.

Let's look on javadoc:
>ThreadLocalMap is a customized hash map suitable only for maintaining thread local values. No operations are exported outside of the ThreadLocal class. The class is package private to allow declaration of fields in class Thread. To help deal with very large and long-lived usages, the hash table entries use WeakReferences for keys. However, since reference queues are not used, stale entries are guaranteed to be removed only when the table starts running out of space.


How it looks from API point of view:
```
[some-thread] -> { someThreadLocal.get() } -> { Thread.currentThread().threadLocals = new ThreadLocalMap(this, someValue); }  
```

How `ThreadLocal` & `ThreadLocalMap` looks:

```
public class ThreadLocal<T> {
	static class ThreadLocalMap {
		...
	}

	ThreadLocalMap getMap(Thread t) {
		return t.threadLocals;
	}

	public T get() {
		Thread t = Thread.currentThread();
		ThreadLocalMap map = getMap(t);
		if (map != null) {
			ThreadLocalMap.Entry e = map.getEntry(this);
			if (e != null)
				return (T) e.value;
		}
		return setInitialValue();
	}

	private T setInitialValue() {
		T value = initialValue();
		Thread t = Thread.currentThread();
		ThreadLocalMap map = getMap(t);
		if (map != null)
			map.set(this, value);
		else
			createMap(t, value);
		return value;
	}

     void createMap(Thread t, T firstValue) {
        t.threadLocals = new ThreadLocalMap(this, firstValue);
    }
	...

    static class ThreadLocalMap {
        static class Entry extends WeakReference<ThreadLocal<?>> {
            ...
        }

        ...
        private Entry[] table;
        ...
    }
}
```

`Thread` class keeps a reference to a `ThreadLocal.ThreadLocalMap` instance, which is built using weak references to the keys. Building the structure in a reverse manner we have avoided thread contention issues altogether as our ThreadLocal can only access the value in the current thread. Also, when the Thread has finished the work, the map can be garbage-collected, so we have also avoided the memory leak issue.

### Reactive programming
How actually we can translate __reactive__ word?
>One who reacts

Reactive programmin - just part of programming paradigm. 
>Reactive programming has been proposed as a way to simplify the creation of interactive user interfaces and near-real-time system animation. For example, in a model–view–controller (MVC) architecture, reactive programming can facilitate changes in an underlying model that are reflected automatically in an associated view.

### Reactive manifesto
[Core ideas](https://www.reactivemanifesto.org/) behind reactive paradigm:

<a href="https://ibb.co/8bMG8Vp"><img src="https://i.ibb.co/jzhq4F2/image.png" alt="image" border="0"></a><br /><a target='_blank' href='https://ru.imgbb.com/'></a><br />

1. Responsive:
The system responds in a timely manner if at all possible.

2. Resilient:
The system stays responsive in the face of failure.

3. Elastic:
The system stays responsive under varying workload. 

4. message driven:
Reactive Systems rely on asynchronous message-passing to establish a boundary between components that ensures loose coupling, isolation and location transparency.

### Observer pattern
One object (Subject) changes the it's state, all of it's dependent objects get notified or updated automaticly.

<a href="https://ibb.co/1JTZy9Q"><img src="https://i.ibb.co/sJqjxFR/image.png" alt="image" border="0"></a>

How it's looks:

<a href="https://ibb.co/PW1bXGp"><img src="https://i.ibb.co/C9VXCKd/image.png" alt="image" border="0"></a>

### Reactive interfaces

Reactive paradigm implemented by several interfaces: `Publisher`, `Subscriber`, `Subscription`.

__Publisher:__

The `Publisher` is a producer of values that may eventually arrive. So, `Publisher` produces values of type `T` to a `Subscriber`.
```
public interface Publisher<T> {
      void subscribe(Subscriber<? super T> s);
}
```

__Subscriber:__

The `Subscriber` subscribes to a `Publisher`, receiving notifications on any new values of type `T` through it's `onNext(T)` method. If there are any errors, it's `onError(Throwable)` method is called. When processing has completed normally, the subscriber’s `onComplete` method is called.
```
public interface Subscriber<T> {
      public void onSubscribe(Subscription s);
      public void onNext(T t);
      public void onError(Throwable t);
      public void onComplete();
}
```

__Subscription:__

When a `Subscriber` first connects to a `Publisher`, it is given a `Subscription` in the `Subscriber#onSubscribe` method. The `Subscription` is arguably the most important part of the whole specification: it enables backpressure. The `Subscriber` uses the `Subscription#request` method to request more data (from `n` to `Long.MAX_VALUE`) or the `Subscription#cancel` method to halt processing. So, `Subscription` is kind of contract between __publisher__ and __subscriver__.

```
public interface Subscription {
      public void request(long n);
      public void cancel();
}
```

_The subscriber controls the flow of data, the rate of processing. The publisher will not produce more data than the amount for which the subscriber has asked. The subscriber can’t be overwhelmed (and if it is ever overwhelmed, it has only itself to blame)._

### Nothing Happens Until You Subscribe
Reactive flow is lazy by default. Calling methods on a `Flux` or `Mono` (the operators) doesn’t immediately trigger the behavior. Instead, a new instance of `Flux` (or `Mono`) is returned, on which you can continue composing further operators. You thus create a chain of operators (or an operator acyclic graph), which represents your asynchronous processing pipeline.

This declarative phase is called __assembly time__.

### Project Reactor, WebFlux or RxJava?
__Reactive paradigm are not designed to handle requests or data faster.__
Their strength lies in their ability to serve more requests at the same time and handle latency operations more efficiently, such as requesting data from a remote server.

Due to resource utilization, the use of [lightweight threads](https://en.wikipedia.org/wiki/Green_threads) (which are based on fewer CPU threads), a non-blocking approach, the reactive approach significantly optimizes the performance of various engineering tasks.

* `Project Reactor` -  [core implementation](https://github.com/reactor/reactor-core) for reactive model paradigm. Reactor has integration with Java conccurent API (Completable Future, Stream and e.t.c). Project Reactor acts like base library.

* `WebFlux` - part of [Spring ecosystem](https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html). Spring Framework uses Reactor for its own reactive support.

* `RxJava` - [implementation](https://github.com/ReactiveX/RxJava) of reactive paradigm without Spring semantics. Acts like `Rroject Reactor` alternative.

* `Netty` - non-blocking [library](https://netty.io/) for IO client-server operations. For example, `WebClient` based on netty threads.

<a href="https://ibb.co/6twz0cY"><img src="https://i.ibb.co/myTk5rJ/image.png" alt="image" border="0"></a>

### Usage in Project Reactor

First of all, let's introduce two types of `Publisher`: [Mono](https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html) and [Flux](https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html):

Mono and Flux are both implements the same interface.

```
public abstract class Mono<T> implements CorePublisher<T>
public abstract class Flux<T> implements CorePublisher<T>
```

_Mono_ represented single element (zero or one), _Flux_ - many elements.

API for creation:

```
Mono<String> single = Mono.just("Single mono element");
Flux<String> many = Flux.just("One", "Two", "Three");
```

Let's create reactive flow of 1 million integers and then subscribe to it:

```
List<Integer> numbers = IntStream.rangeClosed(1, 1_000_000).boxed().collect(Collectors.toList());

Flux<Integer> manyNumbers = Flux.fromIterable(numbers);
// Handle subscription in non-blocking way (don't block current thread)
manyNumbers.subscribe(number -> Math.pow(number, 3));
```

Reactive streams are lazy, they will not be handled until subscription.

From `Flux` point, we can handle such flow as a `Subscriber`:
* `onSubscribe()` – This is called when we subscribe to our stream

* `request(unbounded)` – When we call subscribe, behind the scenes we are creating a Subscription. This subscription requests elements from the stream. In this case, it defaults to unbounded, meaning it requests every single element available

* `onNext()` – This is called on every single element

* `onComplete()` – This is called last, after receiving the last element. There's actually a onError() as well, which would be called if there is an exception, but in this case, there isn't

### Hot vs Cold

A `cold Observable` is entirely lazy and never begins to emit events until someone is actually interested. If there are no observers, Observable is just a static data structure. This also implies that every subscriber receives its own copy of the stream because events are produced lazily but also not likely cached in any way. 

`Hot Observables` are different. After you get a hold of such an Observable it might already be emitting events no matter how many Subscribers they have. Observable pushes events downstream, even if no one listens and events are possibly missed. Whereas typically you have full control over cold Observables, hot Observables are independent from consumers.

<a href="https://ibb.co/wCZxq5h"><img src="https://i.ibb.co/mtrMKf5/image.png" alt="image" border="0"></a>

Examples:

1) __Cold__

For each subscriber new subscription created, publisher emits data for each subscriber independently.
```
public Flux getColdTransmission() {                                            
    return Flux                                                                       
            .interval(Duration.ofSeconds(1))                                          
            .map(s -> "Transmission " + s)                                            
            .doOnSubscribe(v -> System.out.println("Subscription created: " + v));    
}        

var cold = getColdTransmission();       

cold.subscribe(c -> System.out.println("Subscribe 1 : " + c));
Thread.sleep(2000);                            

cold.subscribe(c -> System.out.println("Subscribe 2 : " + c));
Thread.sleep(4000);                                          
```

__Output__:
```
Subscription created: reactor.core.publisher.FluxMap$MapSubscriber@16c0663d
Subscribe 1 : Transmission 0
Subscribe 1 : Transmission 1
Subscription created: reactor.core.publisher.FluxMap$MapSubscriber@c818063
Subscribe 1 : Transmission 2
Subscribe 2 : Transmission 0
Subscribe 1 : Transmission 3
Subscribe 2 : Transmission 1
Subscribe 1 : Transmission 4
Subscribe 2 : Transmission 2
Subscribe 1 : Transmission 5
Subscribe 2 : Transmission 3
```

2) __Hot__

Subscription created only once and after that publisher start emitting data.
```
public ConnectableFlux getHotTransmission() {                              
    return Flux                                                                   
            .interval(Duration.ofSeconds(1))                                       
            .map(s -> "Transmission " + s)                                         
            .doOnSubscribe(v -> System.out.println("Subscription created: " + v)) 
            .publish();                                                           
}  

var hot = getHotTransmission();                              
hot.subscribe(c -> System.out.println("Subscribe 1 : " + c));
hot.connect();                                               
                                                             
Thread.sleep(2000);                                          
                                                             
hot.subscribe(c -> System.out.println("Subscribe 2 : " + c));
                                                             
Thread.sleep(4000);                                          
```

__Output__:
```
Subscription created: reactor.core.publisher.FluxMap$MapSubscriber@1d251891
Subscribe 1 : Transmission 0
Subscribe 1 : Transmission 1
Subscribe 1 : Transmission 2
Subscribe 2 : Transmission 2
Subscribe 1 : Transmission 3
Subscribe 2 : Transmission 3
Subscribe 1 : Transmission 4
Subscribe 2 : Transmission 4
Subscribe 1 : Transmission 5
Subscribe 2 : Transmission 5
```

### Hot to make hot publisher?
One way - convert publisher to connectable publisher. For example, in _Project Reactor_ you can convert `Flux` (reactive stream) to `ConnectableFlux`. 

__Connectable Observable__ is an interesting way of coordinating multiple Subscribers and sharing a single underlying subscription.
__Connectable Observable__ is a type of Observable that ensures there exists at most one Subscriber at all times, but in reality there can be many of them sharing the same underlying resource. Difference between observable and connectable observable: __Connectable Observable__ starts emmiting data only when `connect()` invoked.

```
var nonHotYet = Flux.just(1, 2, 3, 4, 5);
var stillNonHotYet = nonHotYet.publish(); // Observable -> Connectable Observable
stillNonHotYet.connect(); // Now observable is hot
```

### Blocking operations in RxJava
Reactive code supposed to be asynchronous and non-blocking. It's the way to build fully reactive flow. But some of __RxJava__ operations and internal stuff is still blocking.

For example, when a __subscription__ is created, then current thread (in which this subscription happened) is blocked until it created.

```
Observable observable = ...
observable.subscribe(); // Current thread is blocked untill subscription happens (all events recieved)
```

Another example:
```
println(“Before”);
Observable
          .range(5, 3)
          .subscribe(i -> println(i));
println(“After”);
```

We are intrested in is the thread that executed each log statement:
```
main: Before
main: 5
main: 6
main: 7
main: After
```

>The order of `println()` statement is also relevant. It is not a surprise that `Before` and `After` messages printed by the main client thread. However, notice that subscription also happened in the client thread and `subscribe()` actually blocked the client thread until all events were received.

### From blocking to non-blocking
RxJava is concurrency-agnostic, and as a matter of fact it does not introduce concurrency on its own. However, some abstractions to deal with threads are exposed to the end user. Also, certain operators cannot work properly without concurrency.

How to make blocking operations fully asynchronous? For such requirements you can use __Schedulers__. 

>Scheduler provides an abstract asynchronous boundary to operators.

In principle it works similarly to `ScheduledExecutorService` from `java.util.concurrent` — it executes arbitrary blocks of code, possibly in the future. However, to meet _Rx contract_, it offers some more fine-grained abstractions. __Schedulers__ are used together with `subscribeOn()` and `observeOn()` operators as well as when creating certain types of __Observables__. A scheduler only creates instances of Workers that are responsible for scheduling and running code. When RxJava needs to schedule some code it first asks Scheduler to provide a Worker and uses the latter to schedule subsequent tasks.

__API usage__:

1) `subscribeOn` is applied to the __subscription__ process. If you place a `subscribeOn` in a chain, it affects the source emission in the entire chain. 

<a href="https://ibb.co/nfT25ZK"><img src="https://i.ibb.co/2Pw2fmp/image.png" alt="image" border="0"></a>

1. `subscribeOn` affects the whole subscription as it subscribe was being called on that _Scheduler_.
2. Creation of subscription

Usage:
```
var someFluxOfIntegers = ...;
var mappedFlux = someFluxOfIntegers
      .filter(v -> v % 2 == 0)
      .map(v -> v * 2)
      .subscribeOn(someScheduler);

mappedFlux.subscribe(); // Starts flux in non-blocking style (on other thread)   
```

No matter when `subscribeOn` placed. It can be placed at the end/start of reactive stream, but affects the whole chain. As example:
```
 Flux
                .just(1, 3424, 2, 2324, 23)
                .filter(v -> {
                    System.out.println("[thread: " + Thread.currentThread().getName() + "] filtering " + v);
                    return v > 0;
                })
                .subscribeOn(scheduler1)
                .map(v -> {
                    System.out.println("[thread: " + Thread.currentThread().getName() + "] mapping " + v);
                    return v * 2;
                })
                .subscribe();
```

And output:
```
[thread: shed1-1] filtering 1
[thread: shed1-1] mapping 1
[thread: shed1-1] filtering 3424
[thread: shed1-1] mapping 3424
[thread: shed1-1] filtering 2
[thread: shed1-1] mapping 2
[thread: shed1-1] filtering 2324
[thread: shed1-1] mapping 2324
[thread: shed1-1] filtering 23
[thread: shed1-1] mapping 23
```

2) `publishOn` affects subsequent operators after `publishOn` - they will be executed on a thread picked from `publishOn`'s scheduler. 

<a href="https://ibb.co/pjj9nML"><img src="https://i.ibb.co/s99MtNV/image.png" alt="image" border="0"></a>

1. Calling thread or main thread which triggers the subscription. That thread (main one or the blue here) which triggers the subscription will be used for all the work, unless and otherwise specified by a scheduler. 

2. So, here after the `publishOn` the execution of the rest of the operations are handed over to the specified _Scheduler_.

Usage:
```
Flux
                .just(1, 3424, 2, 2324, 23)
                .filter(v -> {
                    System.out.println("[thread: " + Thread.currentThread().getName() + "] filtering " + v);
                    return v > 0;
                })
                .publishOn(scheduler1)
                .map(v -> {
                    System.out.println("[thread: " + Thread.currentThread().getName() + "] mapping " + v);
                    return v * 2;
                })
                .subscribe();
```
And output:
```
[thread: main] filtering 1
[thread: main] filtering 3424
[thread: main] filtering 2
[thread: main] filtering 2324
[thread: main] filtering 23
[thread: shed1-1] mapping 1
[thread: shed1-1] mapping 3424
[thread: shed1-1] mapping 2
[thread: shed1-1] mapping 2324
[thread: shed1-1] mapping 23
```

__Schedulers:__

* `Schedulers.newThread()`

### WebFlux thread model
`Project Reactor` provides excellent API for building reactive stuff, but here is one important point. 
Basic idea on non-blocking operations in `WebFlux` [based on Event Loop concept](https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html#webflux-concurrency-model), because of Netty library integration.
Let's dive into details.

>On a “vanilla” Spring WebFlux server (for example, no data access nor other optional dependencies), you can expect one thread for the server and several others for request processing (typically as many as the number of CPU cores).

So, the number of processing threads that handle incoming requests is __related to the number of CPU cores__. If you have 4 cores and you didn’t enable hyper-threading mechanism you would have 4 worker threads in the pool. You can determine the number of cores available to the JVM by using the static method, `availableProcessors` from class `Runtime`: 
```
Runtime.getRuntime().availableProcessors()
```

Netty responsible how to handle, optimizing, and utilize resources, which allow using real non-blocking way of performing operations.

### Netty Thread model
Here is some Netty channel options:

1. Common ChannelOption
2. Epoll ChannelOption
3. KQueue ChannelOption
4. Socket Options

### Thread models

>A thread is a basic unit of CPU utilization, consisting of a program counter, a stack, and a set of registers, ( and a thread ID. )
Traditional (heavyweight) processes have a single thread of control - There is one program counter, and one sequence of instructions that can be carried out at any given time.
Multi-threaded applications have multiple threads within a single process, each having their own program counter, stack and set of registers, but sharing common code, data, and certain structures such as open files.

<a href="https://ibb.co/bFsnh7J"><img src="https://i.ibb.co/xY1Qv3H/image.png" alt="image" border="0"></a>

Here are some types of threads:

1. Green threads (many-to-one threads) model

Green threads (aka _virtual threads_) are threads that are scheduled by a runtime library or virtual machine (VM) instead of natively by the underlying operating system.

>In this model, many user level threads multiplexes to the Kernel thread of smaller or equal numbers. The number of Kernel threads may be specific to either a particular application or a particular machine.

Following diagram shows the many to many model. In this model, developers can create as many user threads as necessary and the corresponding Kernel threads can run in parallels on a multiprocessor.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/vL5Z24V/image.png" alt="image" border="0"></a>

2. One-to-one model

There is one to one relationship of user level thread to the kernel level thread.
This model provides more concurrency than the many-to-one model. 
It also another thread to run when a thread makes a blocking system call. It support multiple thread to execute in parallel on microprocessors.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/N17wWYk/image.png" alt="image" border="0"></a>

3. Many-to-many model

In this model, many user level threads multiplexes to the Kernel thread of smaller or equal numbers. The number of Kernel threads may be specific to either a particular application or a particular machine.

Following diagram shows the many to many model. In this model, developers can create as many user threads as necessary and the corresponding Kernel threads can run in parallels on a multiprocessor.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/bJ06f1s/image.png" alt="image" border="0"></a>

### Cooperative multitasking
[Cooperative multitasking](https://en.wikipedia.org/wiki/Cooperative_multitasking) - style of multithreading programming paradigm. 
There is no context swithicng at all, because each thread decides when to acquire CPU and give a chance to another tasks (in another thread).
In such case [yield operation](https://en.wikipedia.org/wiki/Yield_(multithreading)).

>This type of multitasking is called "cooperative" because all programs must cooperate for the entire scheduling scheme to work.

Anyway, if some thread will fail, then others threads will wait endless time.

### Kotlin Coroutines
Coroutines looks really simmilar to green threads (aka many-to-one thread model).
In general, coroutine is:
>Coroutines are computer program components that generalize subroutines for non-preemptive multitasking, by allowing execution to be suspended and resumed. 
Coroutines are well-suited for implementing familiar program components such as cooperative tasks, exceptions, event loops, iterators, infinite lists and pipes. 
Coroutines are very similar to threads. However, coroutines are cooperatively multitasked, whereas threads are typically preemptively multitasked. This means that coroutines provide concurrency but not parallelism. The advantages of coroutines over threads are that they may be used in a hard-realtime context (switching between coroutines need not involve any system calls or any blocking calls whatsoever).

Official JetBrains [description of Coroutines](https://kotlinlang.org/docs/mobile/concurrency-and-coroutines.html#coroutines):
>Coroutines are light-weight threads that allow you to write asynchronous non-blocking code. 
You can suspend execution and do work on other threads while using a different mechanism for scheduling and managing that work.
Within a coroutine, the processing sequence may be suspended and resumed later. This allows for asynchronous, non-blocking code, without using callbacks or promises. That is asynchronous processing, but everything related to that coroutine can happen in a single thread.

### Suspended functions
Most functions in Kotlin may be marked suspending using the special suspend modifier. There are almost no additional restrictions: regular functions, extension functions, top-level functions, local functions, lambda literals, all these may be suspending functions.

### Continuation-passing style (Callbacks)
The code generated by the Kotlin compiler for suspending functions uses a method named continuation passing style. On the normal direct style, when a function ends it returns to its call site, typically by assigning the program counter with the value saved in the stack.
On the continuation-passing style, the code to execute after a function ends is passed explicitly into the function as an extra parameter, called the __continuation__.

For instance, consider the following pseudo-code:

```
prev_statement
function(a,b)
next_statement
...
```

<a href="https://imgbb.com/"><img src="https://i.ibb.co/YcxhGZT/image.png" alt="image" border="0"></a>

Using the continuation-passing style, the code after the function call is encapsulated into another function, which is passed into the call to function:

<a href="https://imgbb.com/"><img src="https://i.ibb.co/n1qGyKS/image.png" alt="image" border="0"></a>

In this case, the continuation, i.e. next_statement is called explicitly by function.
In Kotlin, the continuation is represented by the following interface:

```
public interface Continuation<in T> {
    public fun resume(value: T)
    public fun resumeWithException(exception: Throwable)
}
```

So, after compilation next function

```
suspend fun foo(arg: Int): Int { ... }
```

Will looks like:

```
public static final Object foo(int arg, @NotNull Continuation var2) { ... }
```

By explicitly passing a reference to the continuation code, the CPS style allows the called function to better control when and how the continuation is called. For instance, it can be called synchronously when the function ends its processing, producing something very similar to what is obtained with the direct style. In this case, the complete flow happens in the same thread or another one:

1)

<a href="https://imgbb.com/"><img src="https://i.ibb.co/fNgM3nm/image.png" alt="image" border="0"></a>

2)

<a href="https://imgbb.com/"><img src="https://i.ibb.co/PxcnMVC/image.png" alt="image" border="0"></a>

So, CPS really looks like callbacks in general:

<a href="https://ibb.co/bvQGsMz"><img src="https://i.ibb.co/3F4Pp9B/image.png" alt="image" border="0"></a>

### How kotlin coroutines works under the hood?
